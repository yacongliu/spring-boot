# 1. SpringBoot介绍

## 1.1 SpringBoot特点

1. SpringBoot涉及的目的就是用来简化Spring应用的初始化搭建以及开发的过程
2. 嵌入的Tomcat，无需部署WAR文件
3. SpringBoot并不是对Spring功能的增强，而是提供了一种快速使用Spring的方式

## 1.2 版本要求

SpringBoot2.0以上要求JDK版本为1.8以上

pom文件中可以修改JDK版本

```
<properties>
		<java.version>1.8</java.version>
</properties>
```

## 1.3 注入SpringBoot启动坐标

```
<!--SpringBoot web组件-->
<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

+ **spring-boot-starter-parent** 

  在pom.xml中引入spring-boot-starter-parent 可以提供dependency management (依赖管理) 引入以后在申明其他dependency的时候就不需要指定version了

+ **spring-boot-starter-web**
  springweb核心组件

+ **spring-boot-maven-plugin**

+ 如果我们要直接Main中启动Spring，那么就需要添加该plugin，否则无法启动

## 1.4 SpringBoot启动器

所谓的启动器其实就是一些jar包的集合。SpringBoot一共提供了44中启动器

+ spring-boot-starter-web  支持全栈式web开发，包括tomcat、springmvc等jar
+ spring-boot-starter-jdbc  支持spring以jdbc的方式操作数据库的jar包集合
+ spring-bootstarter-redis  支持redis键值存储的数据库操作

## 1.5 启动SpringBoot 编写启动类

```
@SpringBootApplication
public class BootdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootdemoApplication.class, args);
	}

}
```

**@SpringBootApplication**  告知Spring该类为启动类

**注意：** 启动器存放的位置。启动器可以放到同级包，或者上一级包，但是不能放到子包！

# 2. SpringBoot Web开发

## 2.1 SpringBoot 整合Servlet

### 2.1.1 通过注解扫描完成对Servlet组件的注册

```
/**
 * SprignBoot 整合Servlet方式一
 */
@WebServlet(name = "FirsetServlet", urlPatterns = "/first")
public class FirstServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("FirstServlet...........");
    }
}
```

以前都是在web.xml中进行配置Servlet。

```
<servlet>
	<servlet-name>FirstServlet</servlet-name>
	<servlet-class>com.mayi.bootdemo.servlet.FirstServlet</servlet-class>
</servlet>

<servlet-mapping>
	<servlet-name>FirstServlet</servlet-name>
	<servlet-url>/first</servlet-url>
</servlet-mapping>

```

因为在SpringBoot中没有web.xml。因此可以使用Servlet3.0注解进行注册Servlet组件。

**编写SpringBoot启动类**

```
@SpringBootApplication
@ServletComponentScan
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
```

**@ServletComponentScan**  SpringBoot启动时会扫描带有@WebServlet注解的类并将其实例化

### 2.1.2 通过方法完成对Servlet组件的注册

```
public class SecondServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("SecondServlet。。。。。。。。。。");
    }
}
```

编写Servlet时不加任何注解，而是在启动类中以方法的形式提供Bean

```
@SpringBootApplication
public class App2 {

    public static void main(String[] args) {
        SpringApplication.run(App2.class, args);
    }

    @Bean
    public ServletRegistrationBean getSecondServletBean() {
        ServletRegistrationBean bean = new ServletRegistrationBean(new SecondServlet(), "/second");
        return bean;
    }
}
```

## 2.2 静态资源访问

在我们开发Web应用的时候，需要引用大量的js、css、图片等静态资源。

默认配置

Spring Boot默认提供静态资源目录位置需置于classpath下，目录名需符合如下规则：

/static

/public

/resources          

/META-INF/resources

举例：我们可以在src/main/resources/目录下创建static，在该位置放置一个图片文件。启动程序后，尝试访问http://localhost:8080/D.jpg。如能显示图片，配置成功。

## 2.3 文件上传

**Html**

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>文件上传</title>
</head>
<body>

<form action="fileUploadController" method="post" enctype="multipart/form-data">
    <label for="upload">上传文件：</label>
    <input type="file" id="upload" name="fileName">
    <input type="submit" value="提交">
</form>

</body>
</html>
```



**编写controller**

```
package com.mayi.bootdemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class FileUploadController {

    @RequestMapping("/fileUploadController")
    public Map<String, Object> upload(MultipartFile fileName) throws IOException {
        Map<String, Object> map = new HashMap<String, Object>(1);
        System.out.println(fileName.getOriginalFilename());
        fileName.transferTo(new File("D:/" + fileName.getOriginalFilename()));

        map.put("msg", "上传成功");

        return map;

    }
}
```

**编写启动类**

```
@SpringBootApplication
public class App3 {

    public static void main(String[] args) {
        SpringApplication.run(App3.class, args);
    }
}
```

**注意：** 在application.properties 配置文件中对文件上传做限制

```
// 设置单个文件上传的大小 默认时10MB
spring.http.mulipart.maxFileSize = 20MB
// 设置一次请求上传文件的总容量
spring.http.mulipart.maxRequestSize = 100MB
```

## 2.4 SpringBoot整合Freemarker

**引入依赖**

```
<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-freemarker</artifactId>
</dependency>
```

这里不需要指定freemarker的版本，因为已经在parent中定义好对应版本。

**ftl模板**

SpringBoot要求模板形式的视图层技术文件必须放到`src/main/resources/templates`  而且目录名称必须为templates！

```
<html>
<head>
    <meta charset="UTF-8"/>
    <title>SpringBoot整合Freemarker</title>
</head>
<bofy>
    <div>
        <table border="1" align="center" width="50%">
            <tr>
                <th>名称</th>
                <th>年龄</th>
                <th>性别</th>
            </tr>

            <tbody>
            <#list list as item>
            <tr>
                <td>${item.name}</td>
                <td>${item.age}</td>
                <td>
                    <#if item.sex="0">
                        男
                    <#else>
                        女
                    </#if>
                </td>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>
</bofy>
</html>
```

**Controller**

```
@Controller
public class FtlController {

    @RequestMapping("/showUser")
    public String showUser(Model model) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Map map1 = new HashMap();
        map1.put("name", "张三");
        map1.put("age", "20");
        map1.put("sex", "0");

        Map map2 = new HashMap();
        map2.put("name", "李白");
        map2.put("age", "22");
        map2.put("sex", "1");

        list.add(map1);
        list.add(map2);

        model.addAttribute("list",list);

        return "userList";

    }
}
```

Controller只能使用@Controller 不能使用@RestController。@RestController提供的时Json格式数据交互，而视图层需要进行页面跳转。





